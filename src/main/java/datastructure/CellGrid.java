package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    CellState[][] cellGrid;
    int rows;
    int columns;

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.columns = columns;

		cellGrid = new CellState[rows][columns];
        
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                cellGrid[i][j] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        try {
            cellGrid[row][column] = element;
        } catch (IndexOutOfBoundsException e) {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public CellState get(int row, int column) {
        try {
            return cellGrid[row][column];
        } catch (IndexOutOfBoundsException e) {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public IGrid copy() {
        CellGrid newCellGrid = new CellGrid(rows, columns, null);

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                newCellGrid.set(i, j, cellGrid[i][j]);
            }
        }

        return newCellGrid;
    }
    
}
